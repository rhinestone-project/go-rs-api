package rs_api

import (
	"errors"
	"strconv"
)

type String struct {
	value string
}

func readEscape(currentPos int, text []rune) (rune, int, error) {
	currentPos++
	chr := text[currentPos]
	switch chr {
	case 'r', 't', 'a', 'f', 'v', 'b', 'n', '\\', '\'', '"':
		return chr, currentPos, nil
	case '0':
		return 0, currentPos, nil
	case '$':
		return chr, currentPos, nil
	case 'u':
		code := make([]rune, 4)
		currentPos++
		for i := 0; i < 4; i++ {
			if c := text[currentPos]; c == 0 {
				return 0, currentPos, errors.New("unexpected EOF")
			} else if ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F') {
				code[i] = c
			} else {
				return 0, currentPos, errors.New("invalid hex digit '" + string(c) + "'")
			}
			currentPos++
		}
		codeValue, err := strconv.ParseInt(string(code), 16, 32)
		if err != nil {
			return 0, currentPos, err
		}
		return rune(codeValue), currentPos, nil
	default:
		return 0, currentPos, errors.New("invalid escape sequence: \\" + string(chr))
	}
}

func NewString(text string, parseEscapes bool) (*String, error) {
	if parseEscapes {
		runes := []rune(text)
		currentPos := 0
		idx := 0
		for currentPos < len(runes) {
			chr := runes[currentPos]
			if chr == '\\' {
				escape, pos, err := readEscape(currentPos, runes)
				if err != nil {
					return nil, errors.New(err.Error() + " at " + strconv.Itoa(pos+1))
				} else {
					currentPos = pos
					runes[idx] = escape
				}
			} else {
				runes[idx] = chr
			}
			currentPos++
			idx++
		}
		text = string(runes[:idx])
	}
	return &String{value: text}, nil
}

func StringOf(text string) *String {
	return &String{value: text}
}

func (s *String) String() string {
	return s.value
}

var stringRT = &ReflectTable{
	Types: []*ReflectType{
		{
			Name: "string",
			Type: TypeString,
		},
	},
	Functions: []*ReflectFunction{
		{
			Name:        "toString",
			ArgsTypes:   []Type{TypeString},
			ReturnTypes: []Type{TypeString},
			Invoker: &FuncInvoker{
				Func: func(env Environment) {
					// return self
				},
			},
		},
	},
}
