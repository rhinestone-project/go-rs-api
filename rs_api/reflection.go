package rs_api

type index struct {
	tableIndex, inTableIndex uint32
}

func (i index) toUInt64() uint64 {
	return uint64(i.tableIndex)<<32 | uint64(i.inTableIndex)
}

func toIndex(i uint64) index {
	return index{
		tableIndex:   uint32((i >> 32) & 0xffffffff),
		inTableIndex: uint32(i & 0xffffffff),
	}
}

type Invoker interface {
	Invoke(env Environment)
}

type FuncInvoker struct {
	Func func(env Environment)
}

func (f *FuncInvoker) Invoke(env Environment) {
	f.Func(env)
}

type ReflectFunction struct {
	Name        string
	ArgsTypes   []Type
	ReturnTypes []Type
	Invoker     Invoker
}

type ReflectType struct {
	Name string
	Type Type
}

type ReflectTable struct {
	Types     []*ReflectType
	Functions []*ReflectFunction
}

func (rt *ReflectTable) findType(name string) (*ReflectType, uint32) {
	for index, it := range rt.Types {
		if it.Name == name {
			return it, uint32(index)
		}
	}
	return nil, 0
}

func (rt *ReflectTable) findFunc(name string, args, returns []Type) (*ReflectFunction, uint32) {
	for index, it := range rt.Functions {
		if it.Name == name {
			match := true
			for i := 0; i < len(args) && match; i++ {
				if args[i] != it.ArgsTypes[i] {
					match = false
				}
			}
			if !match {
				continue
			}
			for i := 0; i < len(returns) && match; i++ {
				if returns[i] != it.ReturnTypes[i] {
					match = false
				}
			}
			if match {
				return it, uint32(index)
			}
		}
	}
	return nil, 0
}

func (rt *ReflectTable) findFuncs(name string, args []Type, tableIndex uint64) ([]*ReflectFunction, []uint64) {
	functions := make([]*ReflectFunction, 0, 1)
	indexes := make([]uint64, 0, 1)
	for index, it := range rt.Functions {
		if it.Name == name {
			match := true
			for i := 0; i < len(args) && match; i++ {
				if args[i] != it.ArgsTypes[i] {
					match = false
				}
			}
			if match {
				functions = append(functions, it)
				indexes = append(indexes, tableIndex|uint64(index))
			}
		}
	}
	return functions, indexes
}

var RT = &rt{
	[]*ReflectTable{
		intRT,
		stringRT,
	},
}

type rt struct {
	tables []*ReflectTable
}

func (r *rt) FindType(name string) (*ReflectType, uint64) {
	for tableIndex, it := range r.tables {
		t, typeIndex := it.findType(name)
		if t != nil {
			return t, index{tableIndex: uint32(tableIndex), inTableIndex: typeIndex}.toUInt64()
		}
	}
	return nil, 0
}

func (r *rt) FindFunc(name string, args, returns []Type) (*ReflectFunction, uint64) {
	for tableIndex, it := range r.tables {
		f, i := it.findFunc(name, args, returns)
		if f != nil {
			return f, index{tableIndex: uint32(tableIndex), inTableIndex: i}.toUInt64()
		}
	}
	return nil, 0
}

func (r *rt) FindFuncs(name string, args []Type) ([]*ReflectFunction, []uint64) {
	functions := make([]*ReflectFunction, 0, 1)
	indexes := make([]uint64, 0, 1)
	for tableIndex, it := range r.tables {
		f, i := it.findFuncs(name, args, uint64(tableIndex)<<32)
		functions = append(functions, f...)
		indexes = append(indexes, i...)
	}
	return functions, indexes
}

func (r *rt) FindTypeByIndex(i uint64) *ReflectType {
	index := toIndex(i)
	if int(index.tableIndex) >= len(r.tables) {
		return nil
	}
	table := r.tables[index.tableIndex]
	if int(index.inTableIndex) >= len(table.Types) {
		return nil
	}
	return table.Types[index.inTableIndex]
}

func (r *rt) FindFuncByIndex(i uint64) *ReflectFunction {
	index := toIndex(i)
	if int(index.tableIndex) >= len(r.tables) {
		return nil
	}
	table := r.tables[index.tableIndex]
	if int(index.inTableIndex) >= len(table.Functions) {
		return nil
	}
	return table.Functions[index.inTableIndex]
}
