package rs_api

import (
	"container/list"
	"testing"
)

type envImpl struct {
	stack *list.List
}

func newEnv() Environment {
	return &envImpl{stack: list.New()}
}

type intPtr struct {
	i Int
}

func (env *envImpl) PushInt(i Int) {
	env.stack.PushFront(&intPtr{i: i})
}

type stringPtr struct {
	s String
}

func (env *envImpl) PushString(s String) {
	env.stack.PushFront(&stringPtr{s: s})
}

func (env *envImpl) PopInt() {
	env.stack.Remove(env.stack.Front())
}

func (env *envImpl) PopString() {
	env.stack.Remove(env.stack.Front())
}

func (env *envImpl) GetInt() *Int {
	e := env.stack.Front()
	return &e.Value.(*intPtr).i
}

func (env *envImpl) GetString() *String {
	e := env.stack.Front()
	return &e.Value.(*stringPtr).s
}

func TestIntToString(t *testing.T) {
	env := newEnv()
	i, err := NewInt("fe", 16)
	if err != nil {
		t.Error(err)
		return
	}
	env.PushInt(*i)

	f, _ := RT.FindFunc("<operator_inc>", []Type{TypeInt}, []Type{TypeInt})
	if f == nil {
		t.Error("reflection not loaded")
		return
	}
	f.Invoker.Invoke(env)

	f, _ = RT.FindFunc("toString", []Type{TypeInt}, []Type{TypeString})
	if f == nil {
		t.Error("reflection not loaded")
		return
	}
	f.Invoker.Invoke(env)

	s := env.GetString()
	if s.value != "255" {
		t.Error("toString(int):(string) -> incorrect behaviour")
		return
	}
}
