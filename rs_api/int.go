package rs_api

import (
	"errors"
	"math/big"
	"strconv"
)

type Int struct {
	value *big.Int
}

func NewInt(text string, base int) (*Int, error) {
	bi := new(big.Int)
	runes := make([]rune, len(text))
	idx := 0
	for _, it := range text {
		if it != '_' {
			runes[idx] = it
			idx++
		}
	}
	text = string(runes[:idx])
	if i, ok := bi.SetString(text, base); ok {
		return &Int{value: i}, nil
	} else {
		return nil, errors.New("cannot cast \"" + text + "\" to integer with base " + strconv.Itoa(base))
	}
}

func IntOf(i *big.Int) *Int {
	return &Int{value: i}
}

func (i *Int) CompareTo(other *Int) int {
	return i.value.Cmp(other.value)
}

func (i *Int) ToInt() int {
	return int(i.value.Int64() & 0xffffffff)
}

func (i *Int) ToBytes() []byte {
	return i.value.Bytes()
}

func (i *Int) String() string {
	return i.value.String()
}

var one = big.NewInt(1)

var Zero = IntOf(big.NewInt(0))
var One = IntOf(one)

var intRT = &ReflectTable{
	Types: []*ReflectType{
		{
			Name: "int",
			Type: TypeInt,
		},
	},
	Functions: []*ReflectFunction{
		{
			Name:        "<operator_inc>",
			ArgsTypes:   []Type{TypeInt},
			ReturnTypes: []Type{TypeInt},
			Invoker: &FuncInvoker{
				Func: func(env Environment) {
					self := env.GetInt()
					value := self.value
					value = value.Add(value, one)
					self.value = value
				},
			},
		},
		{
			Name:        "<operator_dec>",
			ArgsTypes:   []Type{TypeInt},
			ReturnTypes: []Type{TypeInt},
			Invoker: &FuncInvoker{
				Func: func(env Environment) {
					self := env.GetInt()
					value := self.value
					value = value.Sub(value, one)
					self.value = value
				},
			},
		},
		{
			Name:        "toString",
			ArgsTypes:   []Type{TypeInt},
			ReturnTypes: []Type{TypeString},
			Invoker: &FuncInvoker{
				Func: func(env Environment) {
					self := env.GetInt()
					stringValue := String{value: self.String()}
					env.PopInt()
					env.PushString(stringValue)
				},
			},
		},
	},
}
