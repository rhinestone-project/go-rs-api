package rs_api

type Environment interface {
	PushInt(i Int)
	PushString(s String)

	PopInt()
	PopString()

	GetInt() *Int
	GetString() *String
}
