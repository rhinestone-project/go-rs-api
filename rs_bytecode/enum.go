package rs_bytecode

type Type byte

const (
	Nop Type = iota

	LoadConstInt
	LoadConstString

	LoadInt
	LoadString

	StoreInt
	StoreString

	PopInt
	PopString

	DubInt
	DubString

	InvokeNative

	End
)
